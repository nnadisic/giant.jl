using GIANT
using LinearAlgebra
using Plots

# Function similar to Matlab logspace
logrange(x1, x2, n) = (10^y for y in range(log10(x1), log10(x2), length=n))

# Parameters
m = 200
n = 500
r = 10
nbiterhals = 100
nblambdas = 20

W = rand(m, r)
realH = rand(r, n)
M = W * realH

WtM = W' * M
WtW = W' * W


lambdas = collect(logrange(1e-2, 10, nblambdas))
errors = zeros(nblambdas)
sparsities = zeros(nblambdas)
H0 = rand(r, n)
for (it, lambda) in enumerate(lambdas)
    myH = copy(H0)
    for _ in 1:nbiterhals
        halsH!(M, W, myH, WtM, WtW, lambda)
    end
    errors[it] = relreconstructionerror(M, W, myH)
    sparsities[it] = count(>=(1e-4), myH)/length(myH)
end

plot(lambdas, errors)
plot!(twinx(), lambdas, sparsities, color=:red)
