# Test to use while developing

using GIANT
using Plots

# Parameters
m = 200
n = 500
r = 10

realW = rand(m, r)
realH = rand(r, n)
M = realW * realH

# nmf(M, r)
@time W, H, t, e = nmf(M, r)

# display(realW)
# display(realH)
# display(W)
# display(H)

println("Error: $(e[end])")

plot(t, e)
