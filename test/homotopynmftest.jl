# Test to use while developing

using GIANT
using Plots

# Parameters
m = 200
n = 5000
r = 10
k = 5
delta = 1

realW = rand(m, r)
# realH = rand(r, n)
realH = rand_deltaksparse(r, n, k, delta)
M = realW * realH

# Test "currified" shamans
function shamans_defk!(M, W, H)
    # println("M $(size(M)) W $(size(W)) H $(size(H))")
    newH = homotopymatrix(M, W, 5*size(M,2), assignglobal)
    # println("Test")
    H .= newH
    # println("Coucou")
end


# display([sum(realW[:,j]) for j in 1:r])
# display([sum(realH[i,:]) for i in 1:r])

# nmf(M, r)
@time W, H, t, e = nmf(M, r; maxiter=10, updaterW! = halsW!, updaterH! = shamans_defk!)

# display(realW)
# display(realH)
# display(W)
# display(H)

println("Error: $(e[end])")

scatter(t, e)
