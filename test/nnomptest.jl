# Test for NNOMP

using GIANT
using LinearAlgebra

# Parameters
m = 20
r = 10
k = 5
noiselvl = 0.02

A = rand(m, r)
# Normalize columns of A
for col in eachcol(A)
    col ./= norm(col)
end
realx = rand_ksparse_vect(r, k)
b = A * realx

# Add noise to b
noise = randn(m)
noise ./= norm(noise)
noise .*= norm(b) * noiselvl
b .+= noise
b[b .< 0] .= 0 # nonnegativity

# Precompute Gram matrices
AtA = A'*A
Atb = A'*b

# Compute x with nnomp
@time x = nnomp(AtA, Atb, k)

display(hcat(realx, x))
println("Rel error = $(norm(realx - x) / norm(realx))")

if (realx .> 0) == (x .> 0)
    println("Same support :)")
else
    println("Different support :(")
end

# Compute Pareto front
println("\n")
println("Pareto")
@time paretomat = nnomp(AtA, Atb, r-1, returnpareto=true)
display(paretomat)
println("Errors")
display([norm(A*x-b) for x in eachcol(paretomat)]')
