# Testing multiple right-hand sides homotopy algorithm

using GIANT

using LinearAlgebra

# Synthetic data parameters
m = 8
n = 6
r = 5
k = 3
delta = 1
addnoise = false

# Algo parameters
algok = 3

realW = rand(m, r)
realH = rand_deltaksparse(r, n, k, delta)
realHsparsity = sum(realH .> 0)
M = realW * realH

# Add noise to M
if addnoise
    noiseM = randn(m, n)
    noiseM ./= norm(noiseM)
    noiseM .*= norm(M) * 0.01
    M .+= noiseM
    M[M .< 0] .= 0 # Nonnegativity
end

println("Real H")
display(realH)

println("\nCompute H with k-sparse")
@time Hks = homotopymatrix(M, realW, algok, assignksparse)
println("Error: $(relreconstructionerror(M, realW, Hks))")
display(Hks)

println("\nCompute H with greedy selection")
@time Hg = homotopymatrix(M, realW, realHsparsity, assigngreedy)
println("Error: $(relreconstructionerror(M, realW, Hg))")
display(Hg)

println("\nCompute H with global selection")
@time Hb = homotopymatrix(M, realW, realHsparsity, assignglobal)
println("Error: $(relreconstructionerror(M, realW, Hb))")
display(Hb)
