# Test for matrix-NNOMP

using GIANT
using LinearAlgebra

# Parameters
m = 4
n = 6
r = 3
k = 2
deltak = 0
noiselvl = 0.0

A = rand(m, r)
# Normalize columns of A
for col in eachcol(A)
    col ./= norm(col)
end
realX = rand_deltaksparse(r, n, k, deltak)
B = A * realX

# Add noise to b
noise = randn(m, n)
noise ./= norm(noise)
noise .*= norm(B) * noiselvl
B .+= noise
B[B .< 0] .= 0 # nonnegativity

# Compute
@time X = nnompmatrix(A, B, k*n)

println("\nReal X")
display(realX)
println("\nComputed X")
display(X)

println("Rel error = $(norm(realX - X) / norm(realX))")
