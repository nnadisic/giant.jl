# Testing SNPA

using GIANT
using LinearAlgebra

# Parameters
m = 6
n = 8
r = 3

realW = rand(m, r)
realH = hcat(I, rand(r, n-r)) # Identity of size r*r followed by rand columns

# Normalize
for col in eachcol(realH)
    col ./= sum(col)
end
for col in eachcol(realW)
    col ./= sum(col)
end

M = realW * realH


@time J, H = snpa(copy(M), r)

# Given J, recompute W with original M
H = H[J,:]
sort!(J)
W = M[:,J]

display(realW)
display(realH)
display(J)
display(W)
display(H)
