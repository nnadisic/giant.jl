# Test to use while developing

using GIANT
using LinearAlgebra

# Parameters
m = 16
n = 1
r = 6
k = 5

targetk = 1

A = rand(m, r)
realx = rand_ksparse_vect(r, k)
b = A*realx

# Noise
noiseb = randn(m)
noiseb ./= norm(noiseb)
noiseb .*= norm(b) * 0.05
b .+= noiseb
b[b .< 0] .= 0 # Nonnegativity


ata = A'*A
atb = A'*b

println("\nReal x")
display(realx)

println("\n")
@time bestx, nbnodes = arborescent(ata, atb, targetk, returnpareto=false)
println("Arborescent x")
display(bestx)
println("Nb nodes")
display(nbnodes)

println("\n")
@time paretomat, paretonbnodes = arborescent(ata, atb, targetk, returnpareto=true)
println("Pareto")
display(paretomat)
println("Errors")
display([norm(A*x-b) for x in eachcol(paretomat)]')
println("Nb nodes")
display(paretonbnodes)
