# Testing matrix-wise L0 selecton with several column-wise algorithms

using GIANT
using LinearAlgebra

# Synthetic data parameters
m = 8
n = 6
r = 5
k = 3
delta = 1
noiselvl = 0.01

realW = rand(m, r)
for col in eachcol(realW)
    col ./= norm(col)
end
realH = rand_deltaksparse(r, n, k, delta)
realHsparsity = sum(realH .> 0)
M = realW * realH

# Algo parameters
targetq = realHsparsity
mink = 1
maxk = r

# Add noise to M
noiseM = randn(m, n)
noiseM ./= norm(noiseM)
noiseM .*= norm(M) * noiselvl
M .+= noiseM
M[M .< 0] .= 0 # Nonnegativity

println("Real H")
display(realH)

println("\narborescent + selection")
@time Harbo = ksparsematrix(M, realW, targetq, mink)
println("Error: $(relreconstructionerror(M, realW, Harbo))")
display(Harbo)

println("\nHomotopy + selection")
@time Hhomo = homotopymatrix(M, realW, targetq)
println("Error: $(relreconstructionerror(M, realW, Hhomo))")
display(Hhomo)

println("\nNNOMP + selection")
@time Hnnomp = nnompmatwisel0(M, realW, targetq, maxk)
println("Error: $(relreconstructionerror(M, realW, Hnnomp))")
display(Hnnomp)

println("\nNNOMP matrix")
@time Hnnompmat = nnompmatrix(realW, M, targetq)
println("Error: $(relreconstructionerror(M, realW, Hnnompmat))")
display(Hnnompmat)
