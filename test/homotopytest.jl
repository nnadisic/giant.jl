# Test to use while developing

using GIANT

# Parameters
m = 16
n = 1
r = 8
k = 6

A = rand(m, r)
realx = rand_ksparse_vect(r, k)
b = A*realx

sols, breakpoints = homotopy(A, b)
@time sols, breakpoints = homotopy(A, b)

# Nice display
let matsols = zeros(r)
    for sol in sols
        matsols = hcat(sol, matsols)
    end
    println("\nSolution path")
    display(matsols)
end

println("\nReal x")
display(realx)

println("\nBreakpoints")
display(breakpoints)
