using GIANT
using LinearAlgebra

function runtestmipksnnls()
    # Parameters
    m = 200
    r = 20
    k = 10

    # Generate data
    A = rand(m, r)
    realx = rand_ksparse_vect(r, k)
    b = A*realx

    # Add noise
    noise = randn(m)
    noise ./= norm(noise)
    noise .*= norm(b) * 0.01
    b .+= noise
    b[b .< 0] .= 0 # ensure b is nonnegative


    # Run algorithms

    # # Brute force
    # println("\nCompute H with bruteforce")
    # @time bfx, _ = bruteforce(A'*A, A'*b, k)
    # println("Error = $(norm(realx - bfx))")

    # Arbo
    println("\nCompute H with arbo")
    @time arbox, _ = arborescent(A'*A, A'*b, k)
    println("Error = $(norm(realx - arbox))")

    # MIP CPLEX
    println("\nCompute H with MIP CPLEX")
    bigm = 1 # big M of the MIP problem, upper bound of the entries of x
    @time mipx = mipksnnls(A, b, k, bigm)
    println("Error = $(norm(realx - mipx))")

    # MIP Gurobi
    println("\nCompute H with MIP Gurobi")
    bigm = 1 # big M of the MIP problem, upper bound of the entries of x
    @time grbx = mipksnnls(A, b, k, bigm, "gurobi")
    println("Error = $(norm(realx - grbx))")


    println()
    # display(hcat(realx, bfx, arbox, mipx))
    display(hcat(realx, arbox, mipx, grbx))
    display([colsparsity(x) for x in [realx, arbox, mipx, grbx]]')
end


function runtestmipksnnlsmatrix()
    # Parameters
    m = 8
    n = 10
    r = 6
    k = 3

    # Generate data
    W = rand(m, r)
    realH = rand_ksparse(r, n, k)
    M = W*realH

    # Run algo
    mipH = mipksnnlsmatrix(M, W, k, 2)
    display(norm(realH-mipH)/norm(realH))
    display(realH)
    display(mipH)
end


runtestmipksnnls()
# runtestmipksnnlsmatrix()
