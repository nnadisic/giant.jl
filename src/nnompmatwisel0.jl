# nnomp for matrix-wise sparse MNNLS

export nnompmatwisel0

# Main function, builds cost matrix and calls assignement method
function nnompmatwisel0(M::AbstractMatrix{T},
                        W::AbstractMatrix{T},
                        targetk::Integer,
                        maxk::Integer=0,
                        assignmethod::Function=assignglobal) where T <: Real
    # Init
    m, n = size(M)
    _, r = size(W)
    if maxk == 0
        maxk = r
    end
    costmatrix = fill(Solution(Inf, [zero(T)]), r, n)

    WtW = W' * W
    WtM = W' * M

    # Run arborescent for each column of H, and build cost matrix
    for j in 1:n
        sols = nnomp(WtW, WtM[:,j], maxk, returnpareto=true)
        for s in eachcol(sols)
            k = max(sum(s .> 0), 1)
            err = norm(W*s - M[:,j]) ^ 2
            if costmatrix[k,j].err > err
                for i in k:r
                    costmatrix[i,j] = Solution(err, s)
                end
            end
        end
    end

    # Call assignement method and return
    H = assignmethod(costmatrix, targetk, M)
    return H
end
