# Nonnegative Orthogonal Matching Pursuit to solve k-sparse NNLS
# This is a naive implementation

export nnomp, multinnomp, nnompmatrix

# Basic NNOMP
# Solves min_x ||Ax-b||_2^2 s.t. x >= 0 and ||x||_0 <= k
function nnomp(AtA::AbstractMatrix{T},
               Atb::AbstractVector{T},
               k::Integer;
               returnpareto::Bool=false) where T <: Real
    # Constant to compare with 0
    roundoff = 1e-15

    # Precompute stuff
    r = size(AtA, 1)

    # Full support with all entries nonzero
    fullsupport = collect(1:r)

    # Current support
    support = Vector{Integer}()

    # Solution vector
    x = zeros(r)

    # Pareto matrix (each col is an intermediary solution)
    paretomat = zeros(r, k)

    # First selected atom
    maxprod, l = findmax(abs.(Atb))

    while length(support) < k && maxprod > roundoff
        # Update support
        push!(support, l)

        # Update xs with activeset
        complsupport = setdiff(fullsupport, support)
        x[complsupport] .= 0
        x[support] .= activeset(AtA[support,support], Atb[support], x[support])

        # Store x in Pareto matrix if required
        if returnpareto
            paretomat[:,length(support)] .= x
        end

        # Compute OMP criterion
        complsupport = setdiff(fullsupport, support)
        # Break if the support is already full
        if isempty(complsupport)
            break
        end
        maxprod, idx = findmax(abs.(Atb[complsupport] - AtA[complsupport,support] * x[support]))
        # Next selected atom
        l = complsupport[idx]
    end

    if returnpareto
        return paretomat
    else
        return x
    end
end


# Apply NNOMP column-wise on MNNLS problem
function multinnomp(W::AbstractMatrix{T},
                    M::AbstractMatrix{T},
                    k::Int;
                    inputgrammat::Bool=false,
                    sumtoone::Bool=false) where T <: Real
    # If not provided, precompute Gram matrices
    WtW = inputgrammat ? W : W' * W
    WtM = inputgrammat ? M : W' * M
    # Init constants
    r, n = size(WtM)
    H = zeros(r, n)
    # Solve the n NNLS subproblems with nnomp
    for j in 1:n
        H[:,j] .= nnomp(WtW, WtM[:,j], k)
    end
    # Return
    return H
end


# NNOMP modified for matrix-wise q-sparse MNNLS
# Solves min_X ||AX-B||_F^2 s.t. X >= 0 and ||X||_0 <= q
function nnompmatrix(A::AbstractMatrix{T},
                     B::AbstractMatrix{T},
                     q::Integer) where T <: Real
    # Constant to compare with 0
    roundoff = 1e-15

    # Precompute stuff
    m, n = size(B)
    _, r = size(A)
    AtA = A'*A
    AtB = A'*B

    # Init empty support
    support = BitArray(zeros(Bool, r, n))
    cursparsity = 0

    # Solution matrix
    X = zeros(r, n)

    # Init residual and first selected atom
    residual = abs.(AtB)
    maxprod, l = findmax(AtA * residual) # l is a CartesianIndex

    while cursparsity < q && maxprod > roundoff
        # Update support
        support[l] = 1
        cursparsity = sum(X .> roundoff)

        # Update column of Xs with new atom, using activeset
        j = l[2]
        colj = @view X[:,j]
        supj = @view support[:,j]
        colj[.~supj] .= 0
        colj[supj] .= activeset(AtA[supj,supj], AtB[supj,j], colj[supj])

        # Update column of residual
        residual[supj,j] .= 0
        residual[.~supj,j] .= abs.(AtB[.~supj,j] - AtA[.~supj,supj] * colj[supj])

        # Compute OMP criterion
        maxprod, l = findmax(residual)
    end

    return X
end
