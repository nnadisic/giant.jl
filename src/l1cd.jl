# Coordinate descent for L1-penalized NNLS

export l1cd!

# Util function to unbias solutions found with L1-coord desc (in-place)
function debias!(l1H, WtW, WtM)
    for j in 1:size(l1H,2)
        h = @view l1H[:,j]
        Wtm = @view WtM[:,j]
        debiascol!(h, WtW, Wtm)
    end
    return nothing
end

function debiascol!(h, WtW, Wtm)
    h[h .< 1e-12] .= 0 # remove smallest values
    h[1:end], _ = reducednnls(WtW, Wtm, (h .> 0), h)
    return nothing
end


function l1cd!(M::AbstractMatrix{T},
               W::AbstractMatrix{T},
               H::AbstractMatrix{T},
               WtM::AbstractMatrix{T}=Matrix{T}(undef, 0, 0),
               WtW::AbstractMatrix{T}=Matrix{T}(undef, 0, 0);
               lambda::T=zero(T), # coefficient of L1 penalty
               nbiter::Int=100,
               delta::T=1e-4, # stopping criterion on iterates of H
               needdebias::Bool=true) where T <: Number
    # Init
    r = size(H, 1)
    # If needed, compute intermediary values
    if length(WtM) == 0
        WtM = W' * M
    end
    if length(WtW) == 0
        WtW = W' * W
    end

    # Until stopping condition
    it = 1
    eps0 = 0
    epsi = 1
    while it <= nbiter && epsi >= delta * eps0
        nodelta = 0.0
        # Loop on rows of H
        for i in 1:r
            irowH = view(H, i, :)'
            deltaH = max.((WtM[i,:]' - WtW[i,:]' * H .- lambda)
                          / WtW[i,i], -irowH)
            irowH .= irowH .+ deltaH
            nodelta += dot(deltaH, deltaH)
            # Safety procedure
            if iszero(irowH)
                irowH .= 1e-15
            end
        end
        if it == 1
            eps0 = nodelta
        end
        epsi = nodelta

        it += 1
    end

    if needdebias
        debias!(H, WtW, WtM)
    end

    return nothing
end
