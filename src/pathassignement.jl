# Generic assignement methods to build an optimal matrix-wise sparse solution from a solution path

export assignksparse, assignglobal, assigngreedy


# Structure to store a solution and its error
mutable struct Solution{T <: Real}
    err::Float64
    vect::AbstractVector{T}
end


# Takes for each column the k-sparse solution
function assignksparse(costmatrix::Array{Solution{T},2}, k::Integer, M) where T <: Real
    r, n = size(costmatrix)
    H = zeros(r, n)
    for (j, col) in enumerate(eachcol(H))
        col[1:end] .= costmatrix[k,j].vect
    end
    return H
end


# Use global selection to minimize error while respecting matrix-wise sparsity budget
function assignglobal(costmatrix::Array{Solution{T},2},
                      budget::Integer,
                      M::AbstractMatrix{T}) where T <: Real
    r, n = size(costmatrix)
    H = zeros(r, n)

    # Build deltacost matrix, giving for every entry of the cost matrix,
    # the local error gain of selecting this entry
    deltacostmat = zeros(r, n)
    # First row
    for j in 1:n
        deltacostmat[1,j] = (norm(M[:,j]) ^ 2) - costmatrix[1,j].err
    end
    # Next rows
    for i in 2:r
        for j in 1:n
            deltacostmat[i,j] = costmatrix[i-1,j].err - costmatrix[i,j].err
        end
    end

    # Build greedymatrix, giving for for every entry of deltacost, the
    # accumulated gain divided by sparsity cost. Will be updated at each iter.
    greedymatrix = zeros(r, n)
    for i in 1:r
        for j in 1:n
            greedymatrix[i,j] = sum(deltacostmat[1:i,j]) / i
        end
    end

    # Init sorted array to remember the max of each column, to avoid recomputations
    maxtracker = Array{Tuple{Float64,Integer,Integer}}(undef,0)
    for j in 1:n
        val, idx = findmax(greedymatrix[:,j])
        newtuple = (val, idx, j)
        insert!(maxtracker, searchsortedfirst(maxtracker, newtuple), newtuple)
    end


    ### Matrix-wise greedy selection of solutions
    # Index of the solution selected for every column, init at 0
    cursors = zeros(Int, n)
    usedbudget = 0
    while usedbudget < budget
        # Select best entry (it is the last of maxtracker, because always sorted)
        val, seli, selj = pop!(maxtracker)
        # Break if all are 0
        if val == 0
            break
        end
        # Update budget and cursor
        usedbudget += seli - cursors[selj]
        cursors[selj] = seli
        # Update col j of greedymatrix and find new max of col j
        greedymatrix[1:seli,selj] .= 0
        newmaxj = (0.0, 0, 0)
        for i in (seli+1):r
            currentval = sum(deltacostmat[(seli+1):i,selj]) / (i - seli)
            greedymatrix[i,selj] = currentval
            newmaxj = max((currentval, i, selj), newmaxj)
        end
        insert!(maxtracker, searchsortedfirst(maxtracker, newmaxj), newmaxj)
    end

    # Build H with selected solutions
    for j in 1:n
        if cursors[j] != 0
            H[:,j] = costmatrix[cursors[j],j].vect
        end
    end
    return H
end


# Use greedy heuristic to minimize error while respecting matrix-wise sparsity budget
function assigngreedy(costmatrix::Array{Solution{T},2},
                      budget::Integer,
                      M::AbstractMatrix{T}) where T <: Real
    r, n = size(costmatrix)
    H = zeros(r, n)

    # Build deltacost matrix, giving for every entry of the cost matrix,
    # the local error gain of selecting this entry
    deltacostmat = zeros(r, n)
    # First row
    for j in 1:n
        deltacostmat[1,j] = (norm(M[:,j]) ^ 2) - costmatrix[1,j].err
    end
    # Next rows
    for i in 2:r
        for j in 1:n
            deltacostmat[i,j] = costmatrix[i-1,j].err - costmatrix[i,j].err
        end
    end

    # Init sorted array to remember the order of values, to avoid recomputations of max
    maxtracker = Array{Tuple{Float64,Integer,Integer}}(undef,0)
    for j in 1:n
        val = deltacostmat[1,j]
        newtuple = (val, 1, j)
        insert!(maxtracker, searchsortedfirst(maxtracker, newtuple), newtuple)
    end

    # Greedy selection
    cursors = zeros(Int, n) # index of the solution selected for every column, init at 0
    for it in 1:budget
        # Select best entry
        val, seli, selj = pop!(maxtracker)
        # Break if best is 0
        if val == 0
            break
        end
        cursors[selj] += 1
        # Insert next entry of col j in maxtracker, unless we reached end of col
        if seli+1 <= r
            nextvalj = (deltacostmat[seli+1,selj], seli+1, selj)
            insert!(maxtracker, searchsortedfirst(maxtracker, nextvalj), nextvalj)
        end
    end

    # Build H with selected solutions
    for j in 1:n
        if cursors[j] != 0
            H[:,j] = costmatrix[cursors[j],j].vect
        end
    end
    return H
end
