module GIANT

using Requires

include("nmf.jl")
include("nnls.jl")
include("hals.jl")
include("l1cd.jl")
include("ksparsennls.jl")
include("homotopy.jl")
include("pathassignement.jl")
include("ksparsematrix.jl")
include("homotopymatrix.jl")
include("nnomp.jl")
include("nnompmatwisel0.jl")
include("snpa.jl")
include("ssnmf.jl")
include("util.jl")
include("hyperspectral.jl")

# Load MIP methods only if JuMP is loaded (with 'using' or 'import')
function __init__()
    @require JuMP="4076af6c-e467-56ae-b986-b466b2749572" include("mipksnnls.jl")
end


end # module
