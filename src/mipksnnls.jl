# Solving k-sparse NNLS using JuMP framework and generic MIQP solver
export mipksnnls, mipksnnlsmatrix

using JuMP
using CPLEX
using Gurobi

function mipksnnls(A, b, k, bigm, solver="cplex", grbenv=nothing)
    r = size(A, 2)

    model = Model()
    if solver == "cplex"
        model = Model(with_optimizer(CPLEX.Optimizer,
                                     CPX_PARAM_SCRIND=0, # 1 for verbose mode, 0 for no output
                                     CPX_PARAM_THREADS=1, # use only 1 thread
                                     CPX_PARAM_EPAGAP=1e-12, # absolute gap tolerance
                                     CPX_PARAM_EPGAP=1e-12, # relative gap tolerance
                                     CPX_PARAM_EPINT=0.0, # integrality tolerance
                                     ))
    elseif solver == "gurobi"
        model = Model(() -> Gurobi.Optimizer(grbenv))
        set_optimizer_attribute(model, "OutputFlag", 0)
        set_optimizer_attribute(model, "IntFeasTol", 1e-9)
        set_optimizer_attribute(model, "FeasibilityTol", 1e-9)
        set_optimizer_attribute(model, "MIPGap", 1e-12)
    end

    # Solution vector
    @variable(model, x[1:r])
    # Auxiliary variables to enforce k-sparse constraint linearly
    @variable(model, z[1:r], Bin)

    # Constraint of nonnegativity
    @constraint(model, x .>= 0)
    # Constraint of k-sparsity
    @constraint(model, sum(z) <= k)
    @constraint(model, [i in 1:r], x[i] <= bigm*z[i])

    # Objective: minimize the reconstruction error
    @objective(model, Min, 0.5*x'*A'*A*x - b'*A*x)

    JuMP.optimize!(model)
    sol = JuMP.value.(x)

    return sol
end


# Apply mipksnnls on multiple right-hand sides (loop on columns)
function mipksnnlsmatrix(M, W, k, bigm, solver="cplex")
    # If using Gurobi, create Gurobi environment once and for all
    grbenv = nothing
    if solver == "gurobi"
        grbenv = Gurobi.Env()
    end
    n = size(M, 2)
    r = size(W, 2)
    H = zeros(r, n)
    for j in 1:n
        H[:,j] .= mipksnnls(W, M[:,j], k, bigm, solver, grbenv)
    end
    return H
end
