# Homotopy for matrix-wise sparse MNNLS

export homotopymatrix

# Main function, builds cost matrix and calls assignement method
function homotopymatrix(M::AbstractMatrix{T},
                        W::AbstractMatrix{T},
                        targetk::Integer,
                        assignmethod::Function=assignglobal) where T <: Real
    # Init
    m, n = size(M)
    _, r = size(W)
    costmatrix = fill(Solution(Inf, [zero(T)]), r, n)

    # Perform homotopy for each column of H, and build cost matrix
    for j in 1:n
        colm = M[:,j]
        sols, _ = homotopy(W, colm)
        for s in sols
            k = sum(s .> 0)
            err = norm(W*s - colm) ^ 2
            if costmatrix[k,j].err > err
                for i in k:r
                    costmatrix[i,j] = Solution(err, s)
                end
            end
        end
    end

    # Call assignement method and return
    H = assignmethod(costmatrix, targetk, M)
    return H
end
