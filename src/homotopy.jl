# Homotopy algortihm, returns a set of solutions

export homotopy

# TODO: types and comments
function homotopy(A, b)
    # Init
    tol = 1e-12
    r = size(A, 2)
    lambdas = [] # breakpoints
    solutions = []

    AtA = A' * A
    Atb = A' * b

    K = falses(r) # create a BitArray full of 'false' (initial support is empty)

    # first lambda
    nextlambda, j = findmax(Atb)
    nextidx = j # index of the next entry to handle
    nextop = true # what to do with next entry: 'true' for adding to K, 'false' for remove

    while nextlambda > tol
        # add or remove the entry
        K[nextidx] = nextop
        push!(lambdas, nextlambda)

        # compute intermediary values
        a, b, c, d = (zeros(r) for _ = 1:4)
        a[K] = AtA[K,K] \ Atb[K]
        b[K] = AtA[K,K] \ ones(sum(K))
        c[.~K] = AtA[.~K,K] * a[K] - Atb[.~K]
        d[.~K] = AtA[.~K,K] * b[K] - ones(sum(.~K))

        q1 = b .< -tol
        q2 = d .< -tol

        v1 = fill(-Inf, r)
        v1[K.&q1] = a[K.&q1] ./ b[K.&q1]
        max1, k1 = findmax(v1)

        v2 = fill(-Inf, r)
        v2[.~K.&q2] = c[.~K.&q2] ./ d[.~K.&q2]
        max2, k2 = findmax(v2)

        if (max1 > max2) || (max1 == max2 && k1 < k2) # if equality, pick smallest index
            nextlambda = max1
            nextidx = k1
            nextop = false
        else
            nextlambda = max2
            nextidx = k2
            nextop = true
        end

        postprocess_a!(a, AtA, Atb)
        push!(solutions, a)
    end

    return solutions, lambdas
end


function postprocess_a!(a, AtA, Atb)
    a[abs.(a) .< 1e-12] .= 0 # remove smallest values (numerical noise)
    if sum(a .< 0) > 0 # if they are negative entries, recompute with nnls
        a[1:end], _ = reducednnls(AtA, Atb, (abs.(a) .> 0), a)
    end
    return
end
