# Algorithm arborescent: branch-and-bound for exact sparse NNLS

export ksparse_mnnls, ksparse_updtH!, arborescent, bruteforce

using IterTools

# k-Sparse NNLS solver for multiple right-hand sides
# solver can be arborescent (default; branch-and-bound solver) or bruteforce
function ksparse_mnnls(M, W, k; solver=arborescent, sumtoone=false)
    m, n = size(M)
    r = size(W, 2)
    H = zeros(r, n)
    avgnbnodes = ksparse_updtH!(M, W, H, k, solver=solver, sumtoone=sumtoone)
    return H, avgnbnodes
end


# same as above but updates H in place
function ksparse_updtH!(M, W, H, k; solver=arborescent, sumtoone=false)
    WtW = W'*W
    WtM = W'*M
    avgnbnodes = 0
    # Loop on all columns of M and H
    for j in 1:size(H, 2)
        H[:, j], nbnodes = solver(WtW, WtM[:, j], k, sumtoone=sumtoone)
        avgnbnodes += nbnodes/size(H, 2)
    end
    return avgnbnodes
end


# TODO make it type-agnostic
mutable struct BabNode
    sol::Vector{Float64}
    resid::Float64
end

# Sparse NNLS solver for one vector
function arborescent(AtA, Atb, k; returnpareto=false, sumtoone=false)
    r = size(AtA, 1)

    # Init values
    support = collect(1:r)
    nbnodes = [1] # Number of nodes explored
    bze = 1 # biggest zeroed entry (to avoid symmetry in branching)
    bestresid = norm(Atb)
    # Init pareto front
    paretofront = BabNode[]
    for _ in 1:r-k
        push!(paretofront, BabNode(zeros(r), Inf))
    end

    # Root node (unconstrained nnls)
    bestx = activeset(AtA, Atb, sumtoone=sumtoone)
    prevx = copy(bestx)

    # Sort support so that the smallest entries of x are constrained first
    sort!(support, by=i->bestx[i])

    # Call to recursive branch-and-bound
    bestx, bestresid = bab(AtA, Atb, k, support, prevx, bze,
                           bestx, bestresid, paretofront, nbnodes,
                           sumtoone=sumtoone)

    if returnpareto
        paretomat = zeros(r, r-k)
        for j in 1:r-k
            paretomat[:,j] .= paretofront[r-k+1-j].sol
        end
        return hcat(bestx, paretomat), nbnodes[1]
    else
        return bestx, nbnodes[1]
    end
end


# Branch-and-bound recursive function
function bab(AtA, Atb, k, support, prevx, bze, bestx, bestresid, paretofront, nbnodes; sumtoone=false)
    # Get dimensions
    r = size(AtA, 2)
    kprime = length(support)

    # Compute nnls with current support
    x, resid = reducednnls(AtA, Atb, support, prevx; sumtoone=sumtoone)
    nbnodes[1] += 1

    # If current residual is worst than bound, then prune
    if resid >= bestresid
        return bestx, bestresid
    end

    # If sparsity k is reached and residual is better than bound, then return new best sol
    if kprime <= k
        return x, resid
    end

    # Test current k'-sparse best sol and potentially update
    if resid < paretofront[r-kprime+1].resid
        paretofront[r-kprime+1].resid = resid
        paretofront[r-kprime+1].sol .= x
    end

    # If sparsity k is not reached and residual is better than bound, then continue
    # One branch for every remaining entry of the support
    for i in bze:length(support)
        inactive = support[i]
        deleteat!(support, i)
        childbze = max(i, bze)
        bestx, bestresid = bab(AtA, Atb, k, support, x, childbze,
                               bestx, bestresid, paretofront, nbnodes,
                               sumtoone=sumtoone)
        insert!(support, i, inactive)
    end

    return bestx, bestresid
end


# Brute force for k-sparse NNLS
function bruteforce(AtA, Atb, k; sumtoone=false)
    r = size(AtA, 1)

    # Init best values
    bestresid = norm(Atb)
    bestx = zeros(r)
    x0 = zeros(r)

    # Enumerate all possible supports of cardinality k
    for supp in subsets(1:r, k)
        x, resid = reducednnls(AtA, Atb, supp, x0, sumtoone=sumtoone)
        if (resid < bestresid)
            bestresid = resid
            bestx = x
        end
    end

    return bestx, length(subsets(1:r, k))
end
