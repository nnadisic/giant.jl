THIS REPOSITORY WILL NOT BE UPDATED ANYMORE. THE GIANT.JL PROJECT HAS MOVED TO [THIS NEW REPOSITORY](https://gitlab.com/giant-team/giant.jl).

The present repository is a bit of a mess, and it contains the experiments from my PhD thesis along with the actual NMF/NNLS codes.
I will keep it as a reference for my PhD thesis, but I moved the GIANT.jl project to a dedicated repository so it can be developed and used more easily as a proper library.

# GIANT.jl

GIANT Is An NMF Toolbox.

It is a collection of algorithms to solve different variants of Nonnegative Least Squares (NNLS) problems and Nonnegative Matrix Factorization (NMF).
In particular, it focuses on **Sparse NNLS/NMF**, the main topic of my work.

It is still a work in progress, with lots of inconstitencies, small bugs, and badly written code.
I do not recommend to use it as a stable package, but you are welcome to reuse bits of code.
If you have any problem or question when using GIANT, please [contact me](http://nicolasnadisic.xyz/) and I will be happy to help!

GIANT is free software, licensed under the [GNU GPL v3](http://www.gnu.org/licenses/gpl.html).

## A word on NNLS and NMF

### NMF

NMF consists in the following optimization problem.
Given a data matrix  $`M \in \mathbb{R}^{m \times n}`$ and a factorization rank $`r`$, we compute the factors $`W \in \mathbb{R}^{m \times r}_{+}`$ and $`H \in \mathbb{R}^{r \times n}_{+}`$ such that $`M \approx WH`$.
In standard NMF, when using the Frobenius norm as a data fidelity measure, this reduces to
$`\min\limits_{H \geq 0, W \geq 0} \frac{1}{2} \| M - WH \|_F^2`$.

### MNNLS

NMF problems are usually solved by alternating between the optimization of $`W`$ and $`H`$, by iteratively updating one factor while fixing the other.
This leads to a NNLS subproblem with multiple right-hand sides (MNNLS), that is $`\min\limits_{H \geq 0} \frac{1}{2} \| M - WH \|_F^2`$ for the update of $`H`$ (and similarly for $`W`$).

### NNLS

An MNNLS problem can be decomposed into $`n`$ independent NNLS subproblems of the form
$`\min\limits_{x \geq 0} \frac{1}{2} \| Ax - b \|_2^2`$
where $`H(:,j)`$, $`W`$, and $`M(:,j)`$ correspond respectively to $`x`$, $`A`$, and $`b`$.
Many algorithms for NMF are thus based on NNLS algorithms.

### k-sparse NNLS

The k-sparse constraint in NNLS problems states that the solution must have at most k non-zero entries,
$`\min\limits_{x \geq 0} \frac{1}{2} \| Ax - b \|_2^2 \text{ s.t. } \|x\|_0 \leq k`$


## Getting started

Once cloned, enter the project's folder and start Julia.

Type `]` to enter Pkg mode, then type `activate .` and then `instantiate` to install the necessary packages. Then type backspace to leave Pkg mode.

To run a test:
`julia> include("test/<nameoftest>.jl")`

The first run will be slow, as everything needs to be compiled.
For performance measures, always run scripts twice.

The datasets present in this repository come from [this website](http://lesun.weebly.com/hyperspectral-data-set.html).
They are stored here using Git LFS.
Make sure to [install Git LFS](https://git-lfs.github.com/) if you want to download them via Git.
You can also download them via the [web interface](https://gitlab.com/nnadisic/giant.jl/-/tree/master/xp/data).

## Algorithms

### NNLS
- HALS (hierarchical alternating least squares) is a standard algorithm for MNNLS, based on block-wise coordinate descent. It's one the most used approaches for NMF.
- NNLS is an active-set method to solve NNLS problems exactly.

### Sparse NNLS
- arborescent (k-sparse NNLS)
- Homotopy

### Separable NMF
- SNPA
- Brassens (Sparse Separable NMF)

### Other
- Hyperspectral
- Util

### MIP for k-sparse NNLS
`src/mipksnnls.jl` is a program to solve k-sparse NNLS through mixed-integer programming, using [JuMP](https://jump.dev/), a modeling language for mathematical optimization.
As a backend solver, either [CPLEX](https://github.com/jump-dev/CPLEX.jl) or [Gurobi](https://github.com/jump-dev/Gurobi.jl) can be used.
Both solvers are commercial, but free licenses are available for education and research.
To avoid cluttering GIANT with these heavy dependencies, I did not include them.

To use `mipksnnls.jl`, please install JuMP, either (or both) solver, and the corresponding Julia wrapper.
Then, load JuMP (`using JuMP`) and the functions will be loaded when loading GIANT.


## Experiments

### Matrix-wise L0-constrained MNNLS

To reproduce the experiments from the paper "[Matrix-wise L0-constrained Sparse Nonnegative Least Squares](https://arxiv.org/abs/2011.11066)" by N. Nadisic et al, use the script `xp/matwise-l0-nnls/runxp.jl`.
[This commit](https://gitlab.com/nnadisic/giant.jl/-/tree/b2598acd9fd97b99aeac5ab03ce4b890e2c5f62a) corresponds to the exact version of the code we used for our paper.


### Biobjective k-sparse NNLS

To reproduce the experiments from the paper "Exact Biobjective k-Sparse Nonnegative Least Squares" by N. Nadisic et al, use the script `xp/pareto-ksparse/runxp.jl`.


