# Test on hyperspectral datasets

using GIANT

using MAT, LinearAlgebra, Plots

### Initialize parameters ############################################
# CBCL
# r = 49
# datafile = "xp/data/cbclim.mat"
# varname = "M" # name of the variable of the file corresponding to input matrix
# nbpixelsrow = 19
# nbpixelscol = 19
# nbimgperrow = 7
# bw=true

# Urban
r = 6
datafile = "xp/data/Urban.mat"
varname = "A"
nbpixelsrow = 307
nbpixelscol = 307
nbimgperrow = 6
bw=false

######################################################################

# Read matrix
data = matopen(datafile)
M = Array(read(data, varname)')

# Run NMF
nmf(M,r)
@time W, H, t, e = nmf(M,r)

# Results
display(norm(M - W * H) / norm(M))
displayabundancemap(H, nbpixelsrow, nbpixelscol, "out.png", bw=bw, nbimgperrow=nbimgperrow)

# Plot error / time
# plot(t, e, yaxis=:log)
