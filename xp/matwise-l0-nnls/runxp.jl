# Experiments on facial and hyperspectral image

using GIANT

using LinearAlgebra
using MAT
using Statistics

# Load data metadata and parameters
include("../xpmetadata.jl")


function runxp(p::XpParams, nbruns=10)
    # Announce
    println("\nProcessing $(uppercase(p.name))")

    # Read data and dictionnary
    data = matopen(p.datafile)
    M = Array(read(data, p.datavarname))
    dict = matopen(p.dictfile)
    W = Array(read(dict, p.dictvarname))

    # Transpose data if necessary
    if p.transposedata
        M = M'
    end
    if p.transposedict
        W = W'
    end

    # Normalized W for NNOMP
    normW = copy(W)
    for col in eachcol(normW)
        col ./= norm(col)
    end

    # Get rank and sparsity target
    r = size(W,2)
    k = p.k
    matk = p.matk
    lambda = p.lambda

    # display(size(M))
    # display(size(W))
    # display(k)
    # display(matk)

    # One run to compile all functions, get errors, sparsity and images
    as_err, l1_err, hcw_err, hs_err, ompcw_err, ompg_err, omps_err, arbocw_err, arbos_err = [0 for _ in 1:9]
    as_spar, l1_spar, hcw_spar, hs_spar, ompcw_spar, ompg_spar, omps_spar, arbocw_spar, arbos_spar = [0 for _ in 1:9]

    # Active-set for NNLS (no sparsity)
    asH, _ = ksparse_mnnls(M, W, r)
    as_err = relreconstructionerror(M, W, asH)
    as_spar = colsparsity(asH)

    # L1-CD
    l1H = copy(asH)
    l1cd!(M, W, l1H, lambda=lambda)
    l1_err = relreconstructionerror(M, W, l1H)
    l1_spar = colsparsity(l1H)

    # Homotopy column-wise k-sparse
    hcwH = homotopymatrix(M, W, k, assignksparse)
    hcw_err = relreconstructionerror(M, W, hcwH)
    hcw_spar = colsparsity(hcwH)

    # Homotopy + selection
    hsH = homotopymatrix(M, W, matk, assignglobal)
    hs_err = relreconstructionerror(M, W, hsH)
    hs_spar = colsparsity(hsH)

    # NNOMP column-wise k-sparse
    ompcwH = multinnomp(normW, M, k)
    ompcw_err = relreconstructionerror(M, normW, ompcwH)
    ompcw_spar = colsparsity(ompcwH)

    # NNOMP global q-sparse
    ompgH = nnompmatrix(normW, M, matk)
    ompg_err = relreconstructionerror(M, normW, ompgH)
    ompg_spar = colsparsity(ompgH)

    # NNOMP + selection
    ompsH = nnompmatwisel0(M, normW, matk)
    omps_err = relreconstructionerror(M, normW, ompsH)
    omps_spar = colsparsity(ompsH)

    # arbo column-wise k-sparse
    arbocwH, _ = ksparse_mnnls(M, W, k)
    arbocw_err = relreconstructionerror(M, W, arbocwH)
    arbocw_spar = colsparsity(arbocwH)

    # arbo + selection
    arbosH = ksparsematrix(M, W, matk)
    arbos_err = relreconstructionerror(M, W, arbosH)
    arbos_spar = colsparsity(arbosH)

    # Save abundance maps
    npr = p.nbpixelsrow
    npc = p.nbpixelscol
    nir = p.nbimgperrow
    bw = p.bw
    displayabundancemap(asH, npr, npc, "out-$(p.name)-as.png", bw=bw, nbimgperrow=nir)
    displayabundancemap(l1H, npr, npc, "out-$(p.name)-l1.png", bw=bw, nbimgperrow=nir)
    displayabundancemap(hcwH, npr, npc, "out-$(p.name)-hcw.png", bw=bw, nbimgperrow=nir)
    displayabundancemap(hsH, npr, npc, "out-$(p.name)-hs.png", bw=bw, nbimgperrow=nir)
    displayabundancemap(ompcwH, npr, npc, "out-$(p.name)-ompcw.png", bw=bw, nbimgperrow=nir)
    displayabundancemap(ompgH, npr, npc, "out-$(p.name)-ompg.png", bw=bw, nbimgperrow=nir)
    displayabundancemap(ompsH, npr, npc, "out-$(p.name)-omps.png", bw=bw, nbimgperrow=nir)
    displayabundancemap(arbocwH, npr, npc, "out-$(p.name)-arbocw.png", bw=bw, nbimgperrow=nir)
    displayabundancemap(arbosH, npr, npc, "out-$(p.name)-arbos.png", bw=bw, nbimgperrow=nir)



    # Various runs to get better runtime estimation
    astime, l1time, hcwtime, hstime, ompcwtime, ompgtime, ompstime, arbocwtime, arbostime =
        [zeros(nbruns) for _ in 1:9]
    for run in 1:nbruns
        print("$(run) ")
        # Active-set for NNLS (no sparsity)
        astime[run] = @elapsed asH = matrixactiveset(W, M)
        # L1-CD
        l1H = copy(asH)
        l1time[run] = @elapsed l1cd!(M, W, l1H, lambda = lambda)
        # Homotopy column-wise k-sparse
        hcwtime[run] = @elapsed hcwH = homotopymatrix(M, W, k, assignksparse)
        # Homotopy + selection
        hstime[run] = @elapsed hsH = homotopymatrix(M, W, matk, assignglobal)
        # NNOMP column-wise k-sparse
        ompcwtime[run] = @elapsed ompcwH = multinnomp(normW, M, k)
        # NNOMP global q-sparse
        ompgtime[run] = @elapsed ompgH = nnompmatrix(normW, M, matk)
        # NNOMP + selection
        ompstime[run] = @elapsed ompsH = nnompmatwisel0(M, normW, matk)
        # arbo column-wise k-sparse
        arbocwtime[run] = @elapsed arbocwH, _ = ksparse_mnnls(M, W, k)
        # arbo + selection
        arbostime[run] = @elapsed arbosH = ksparsematrix(M, W, matk)
    end

    # Output results
    println("")
    println("Algo\tAS\tL1\tHcw\tHs\tOMPcw\tOMPg\tOMPs\tARBOcw\tARBOs")
    print("Time")
    for t in [astime, l1time, hcwtime, hstime, ompcwtime, ompgtime, ompstime, arbocwtime, arbostime]
        print("\t$(round(median(t), digits=2))")
    end
    print("\nError")
    for e in [as_err, l1_err, hcw_err, hs_err, ompcw_err, ompg_err, omps_err, arbocw_err, arbos_err]
        print("\t$(round(e*100, digits=2))")
    end
    print("\nSpar")
    for s in [as_spar, l1_spar, hcw_spar, hs_spar, ompcw_spar, ompg_spar, omps_spar, arbocw_spar, arbos_spar]
        print("\t$(round(s, digits=2))")
    end
    println()
end


runxp(paramcbcl)
runxp(paramfrey)
runxp(paramkuls)
runxp(paramjasper)
runxp(paramjasper_b)
runxp(paramsamson)
runxp(paramurban)
runxp(paramcuprite)
