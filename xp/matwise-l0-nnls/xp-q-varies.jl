# When the sparsity target q varies
using GIANT

using LinearAlgebra
using MAT
using Statistics
using Plots

# Load data metadata and parameters
include("../xpmetadata.jl")


function runxp_q(p::XpParams, nbq=10, nbruns=5; qmax=0)
    # Announce
    println("\nProcessing $(uppercase(p.name))")

    # Read data and dictionnary
    data = matopen(p.datafile)
    M = Array(read(data, p.datavarname))
    dict = matopen(p.dictfile)
    W = Array(read(dict, p.dictvarname))

    # Transpose data if necessary
    if p.transposedata
        M = M'
    end
    if p.transposedict
        W = W'
    end

    # Normalized W for NNOMP
    normW = copy(W)
    for col in eachcol(normW)
        col ./= norm(col)
    end

    # Get dimensions
    m, n = size(M)
    _, r = size(W)
    k = p.k

    # Compute the range of values of q
    if qmax == 0
        qmax = round(Int, n*r)
    end
    qrange = [round(Int, x) for x in range(1, qmax, length=nbq)]

    astime, hstime, ompgtime, ompstime, arbostime =
        [zeros(nbq, nbruns) for _ in 1:5]

    # Run for each value of q
    for (it, q) in enumerate(qrange)
        print("q=$(q) --- ")
        # Various runs to get better runtime estimation
        for run in 1:nbruns
            print("$(run) ")
            # Active-set for NNLS (no sparsity)
            astime[it, run] = @elapsed asH = matrixactiveset(W, M)
            # Homotopy + selection
            hstime[it, run] = @elapsed hsH = homotopymatrix(M, W, q, assignglobal)
            # NNOMP global q-sparse
            ompgtime[it, run] = @elapsed ompgH = nnompmatrix(normW, M, q)
            # NNOMP + selection
            ompstime[it, run] = @elapsed ompsH = nnompmatwisel0(M, normW, q)
            # arbo + selection
            arbostime[it, run] = @elapsed arbosH = ksparsematrix(M, W, q)
        end
    println()
    end

    # Output results
    pl = plot(qrange, vec(minimum(astime, dims=2)), label="AS")

    # Matrix-wise methods
    labels = ["Hs", "OMPg", "OMPs", "ARBOs"]
    i = 1
    for t in [hstime, ompgtime, ompstime, arbostime]
        plot!(pl, qrange, vec(minimum(t, dims=2)), label=labels[i])
        i += 1
    end
    display(pl)
    savefig(pl, "result-$(p.name).png")
    return [astime, hstime, ompgtime, ompstime, arbostime]
end


# output = runxp_q(paramcbcl)
# output = runxp_q(paramfrey)
# output = runxp_q(paramkuls)
output_jasper = runxp_q(paramjasper, qmax=30000)
# output = runxp_q(paramjasper_b)
# output = runxp_q(paramsamson)
output_urban = runxp_q(paramurban, qmax=200000)
# output = runxp_q(paramcuprite)
