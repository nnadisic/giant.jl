using GIANT

using LinearAlgebra
using MAT
using Statistics
using Plots

# Load data metadata and parameters
include("../xpmetadata.jl")


function runxpnnomp(p::XpParams)
    # Announce
    println("\nProcessing $(uppercase(p.name))")

    # Read data and dictionnary
    data = matopen(p.datafile)
    M = Array(read(data, p.datavarname))
    dict = matopen(p.dictfile)
    W = Array(read(dict, p.dictvarname))

    # Transpose data if necessary
    if p.transposedata
        M = M'
    end
    if p.transposedict
        W = W'
    end

    # Get dimensions, rank, and sparsity target
    m, n = size(M)
    r = size(W,2)
    k = p.k
    matk = p.matk

    # Parameters to print abundance maps
    npr = p.nbpixelsrow
    npc = p.nbpixelscol
    nir = p.nbimgperrow
    bw = p.bw

    # Normalize and save coef
    normcoef = zeros(r)
    for j in 1:r
        normcoef[j] = norm(W[:,j])
        W[:,j] ./= normcoef[j]
    end

    # Run
    @time gH = nnompmatrix(W, M, k*n)
    println("$(round(relreconstructionerror(M, W, gH)*100, digits=3)) $(round(colsparsity(gH), digits=3))")

    @time sH = nnompmatwisel0(M, W, k*n)
    println("$(round(relreconstructionerror(M, W, sH)*100, digits=3)) $(round(colsparsity(sH), digits=3))")
end


runxpnnomp(paramcbcl)
runxpnnomp(paramfrey)
runxpnnomp(paramkuls)
runxpnnomp(paramjasper)
runxpnnomp(paramsamson)
# runxpnnomp(paramurban)
# runxpnnomp(paramcuprite)
