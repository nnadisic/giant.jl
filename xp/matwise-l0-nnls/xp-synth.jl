# Synthetic XP to quantify how the variants of matrix-wise selection recover the true solutions

using GIANT

using LinearAlgebra
using Statistics
using Plots

# Function similar to Matlab logspace
logrange(x1, x2, n) = collect(10^y for y in range(log10(x1), log10(x2), length = n))

# Return the proportion of columns having a common support between mat1 and mat2
function percentcommonsupport(mat1, mat2, tol=0)
    nbrow, nbcol = size(mat1)
    if (nbrow, nbcol) != size(mat2)
        error("Matrices need to have the same dimensions.")
    end
    nbmatch = 0
    for j in 1:nbcol
        if (mat1[:,j] .> tol) == (mat2[:,j] .> tol)
            nbmatch += 1
        end
    end
    return 100 * nbmatch / nbcol
end

function generatedata(m, n, r, k, deltak, illcond)
    realW = rand(m, r)
    # Make W ill-conditioned
    if illcond
        f = svd(realW)
        s = Diagonal(logrange(1e-4, 1, r))
        realW = f.U * s * f.Vt
        realW[realW.<0] .= 0
    end
    # Generate sparse H
    realH = rand_deltaksparse(r, n, k, deltak)
    realHsparsity = sum(realH .> 0)
    # Compute M and return
    M = realW * realH
    return M, realW, realH, realHsparsity
end

function runxp_synth(; m=100, n=1000, r=6, k=3, deltak=1,
                     noisemin=0.0001, noisemax=0.2, nbnoise=20,
                     illcond=false)
    # Synthetic data parameters
    noiselvls = logrange(noisemin, noisemax, nbnoise)
    # noiselvls = range(0, noisemax, nbnoise)

    # Generate data and noise
    realM, realW, realH, targetq = generatedata(m, n, r, k, deltak, illcond)
    noiseM = randn(m, n)
    noiseM ./= norm(noiseM)

    println(size(realM))

    # Normalized W for NNOMP
    normW = copy(realW)
    for col in eachcol(normW)
        col ./= norm(col)
    end

    # Init output
    hsres, ompgres, ompsres, arbosres = [zeros(nbnoise) for _ in 1:4 ]

    # Run for different levels of noise
    for (it, nl) in enumerate(noiselvls)
        print("$(it) ")
        # Add noise to M
        curnoise = noiseM * norm(realM) * nl
        M = realM + curnoise
        M[M.<0] .= 0 # Nonnegativity

        hsH = homotopymatrix(M, realW, targetq)
        hsres[it] = percentcommonsupport(realH, hsH)

        ompgH = nnompmatrix(normW, M, targetq)
        ompgres[it] = percentcommonsupport(realH, ompgH)

        ompsH = nnompmatwisel0(M, normW, targetq, r)
        ompsres[it] = percentcommonsupport(realH, ompsH)

        arbosH = ksparsematrix(M, realW, targetq, 1)
        arbosres[it] = percentcommonsupport(realH, arbosH)
    end
    println()
    return noiselvls, hsres, ompgres, ompsres, arbosres
end

labels = ["Hs" "OMPg" "OMPs" "ARBOs"]

nl, hs, ompg, omps, arbo = runxp_synth()
res1 = hcat(hs, ompg, omps, arbo)
p1 = plot(nl, res1, label=labels)

nl, hs, ompg, omps, arbo = runxp_synth(n=200)
res2 = hcat(hs, ompg, omps, arbo)
p2 = plot(nl, res2, label=labels)

nl, hs, ompg, omps, arbo = runxp_synth(n=100)
res3 = hcat(hs, ompg, omps, arbo)
p3 = plot(nl, res3, label=labels)

nl, hs, ompg, omps, arbo = runxp_synth(illcond=true)
res4 = hcat(hs, ompg, omps, arbo)
p4 = plot(nl, res4, label=labels)

nl, hs, ompg, omps, arbo = runxp_synth(n=200, illcond=true)
res5 = hcat(hs, ompg, omps, arbo)
p5 = plot(nl, res5, label=labels)

nl, hs, ompg, omps, arbo = runxp_synth(n=100, illcond=true)
res6 = hcat(hs, ompg, omps, arbo)
p6 = plot(nl, res6, label=labels)
