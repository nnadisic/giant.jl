using GIANT

using Random: seed!
using LinearAlgebra
using MAT
using Statistics
using Plots

# Function similar to Matlab logspace
logrange(x1, x2, n) = (10^y for y in range(log10(x1), log10(x2), length=n))

# Load data metadata and parameters
include("../xpmetadata.jl")


function runxpl1(p::XpParams, lambdamin::T, lambdamax::T, nblambdas::Int) where T<:AbstractFloat
    # Announce
    println("\nProcessing $(uppercase(p.name))")

    # Read data and dictionnary
    data = matopen(p.datafile)
    M = Array(read(data, p.datavarname))
    dict = matopen(p.dictfile)
    W = Array(read(dict, p.dictvarname))

    # Transpose data if necessary
    if p.transposedata
        M = M'
    end
    if p.transposedict
        W = W'
    end

    # Get rank and sparsity target
    r = size(W,2)
    k = p.k
    matk = p.matk

    # Save abundance maps
    npr = p.nbpixelsrow
    npc = p.nbpixelscol
    nir = p.nbimgperrow
    bw = p.bw

    # Precompute Gram matrices
    WtM = W' * M
    WtW = W' * W

    # Init H with active-set NNLS with no sparsity constraint
    H0 = matrixactiveset(W, M)

    # Run
    lambdas = [0 ; collect(logrange(lambdamin, lambdamax, nblambdas-1))]
    errors, sparsities = [zeros(nblambdas) for _ in 1:2]
    # Run for every lambda
    for (it, lambda) in enumerate(lambdas)
        l1H = copy(H0)
        l1cd!(M, W, l1H, WtM, WtW, lambda=lambda)
        # Store error and sparsity
        errors[it] = relreconstructionerror(M, W, l1H)
        sparsities[it] = colsparsity(l1H)
        # displayabundancemap(l1H, npr, npc, "out-$(p.name)-l1hals-$(it).png", bw=bw, nbimgperrow=nir)
    end

    # Plot avoiding lambda=0 (because of log scale...)
    # myplot = plot(lambdas[2:end], errors[2:end], xaxis=:log)
    # plot!(twinx(myplot), lambdas[2:end], sparsities[2:end], color=:red, xaxis=:log)
    # display(myplot)

    display(hcat(lambdas, errors, sparsities))

end


runxpl1(paramcbcl, 155.0, 165.0, 10) # lambda = 160
# runxpl1(paramfrey, 18.8, 19.0, 10) # lambda = 18.9
# runxpl1(paramkuls, 0.06, 0.07, 10) # lambda = 0.068
# runxpl1(paramjasper, 295.0, 305.0, 10) # lambda = 304
# runxpl1(paramjasper_b, 1430.0, 1440.0, 10) # lambda = 1431
# runxpl1(paramsamson, 0.06, 0.07, 10) # lambda = 0.065
# runxpl1(paramurban, 1.42e5, 1.43e5, 10) # lambda = 142000
# runxpl1(paramcuprite, 2.9, 3.0, 10) # lambda = 2.93
