% function [zV,V,theta,err2,lst_add_rm] = AS_NNLS(Hy,HH,z,theta,err2,VERB)
% function [zV,V,theta,err2,lst_add_rm] = AS_NNLS(Hy,HH,z,theta_uls,err2_uls,VERB,z_uls)
%
% This program is a supplementary material to the paper:
%
% Non-Negative Orthogonal Greedy algorithms, by T.T. Nguyen, J. Idier, 
% C. Soussen, and E.-H. Djermoune. 
%
% https://hal.archives-ouvertes.fr/hal-02049424
%
% Goal: compute the NNLS solution for the augmented problem:
%
%          argmin || y- H z ||_2^2 s.t. z>=0 
%
% for a normalized dictionary H from the ULS solution of 
%
%          argmin || y - \tilde{H} z ||_2^2
%
% where \tilde{H} is deduced from H by removing the last column of H.
% It is assumed that the latter ULS solution is non-negative (it therefore
% identifies with the NNLS solution related to \tilde{H}).
%
% Moreover, when there are 7 input arguments, the 7th (optional) input is the 
% ULS solution z_uls related to H.
%
% INPUTS
%
% Hy: vector H'*y, vector of size (n+1) x 1, with n+1 = size(HH,2)
% HH: matrix H'*H, of size (n+1) x (n+1)
% z: vector of size n x 1, ULS solution of argmin_z || y-\tilde{H}z ||_2^2, 
%    z is assumed to be > 0 
% VERB = 1 to save the list of indice of added/removed atoms (=0 by default)
%
% -------------------------
% Case of 6 input arguments:
% -------------------------
% theta: inverse Gram matrix (\tilde{H}'*\tilde{H})^-1, of size n x n
% err2: squared error || y-\tilde{H}z ||_2^2
%
% -------------------------
% Case of 7 input arguments:
% -------------------------
% theta_uls: inverse Gram matrix (H'*H)^-1, of size (n+1) x (n+1)
% err2_uls: squared error || y-z_uls ||_2^2
% Optional argument z_uls :
% ULS solution corresponding to current support, vector of size (n+1) x 1
%
% 
% OUTPUTS
%
% zV>0: non-zero entries of the NNLS solution of the augmented problem,
%       corresponding to H. Vector zV is of size k <= (n+1)
% V:    support of the NNLS solution of the augmented problem, vector of
%       length k <= n+1. 
%       Each element V(i) is in {1,...,n+1} 
% theta: updated inverse Gram matrix corresponding to subset V, of size k x k
% err2: updated squared error || y - H z||_2^2 corresponding to the 
%       NNLS solution
% lst_add_rm: list of the locations of added (+i) or removed (-i) atoms 
%             during the call to AS_NNLS, with i \in {1,...,n+1}
%
%
% Date: Feb. 5, 2019
% Last modified: Feb. 5, 2019
% Authors: T.T. Nguyen, J. Idier, C. Soussen, and E.-H. Djermoune
%----------------------------------------------------------------------
% ******  Copyright (c) 2019   *******
% ******  License CeCILL       *******
%----------------------------------------------------------------------

function [zV,V,theta,err2,lst_add_rm] = AS_NNLS(Hy,HH,z,theta,err2,VERB,varargin)

    if nargin==7,
        z_uls = varargin{1}; % ULS solution corresponding to current
                             % support, vector of size nx1 is given as input
                             
        % in this case, err2 refers to the squared error related to H and z_uls
    elseif nargin~=6,
        error('*** AS_NNLS: 6 or input arguments expected ***');
        % in this case, err2 refers to the squared error related to \tilde{H} and z
    end
    
    lst_add_rm = []; 
        
    % If VERB = 1: saves the list of added / removed atoms during the
    % call to AS_NNLS, indices between 1 and size(H,2) for additions
    % indices between -1 and -(size(H,2)) for removals
    
    k = size(HH,2)-1;
    V = 1:k;           % current subset {1,...,n} of {1,...,n+1}

    zV = z;            % k nonzero entries of the ULS solution related to \tilde{H}
    
    l = k+1;           % location of the first atom to enter the subset
                       % in 1st iteration in dictionary H
                       % = column n+1

    Vall = 1:(k+1);    % all atoms from full dictionary

    maxprod = 1;       % the NNOG mechanism guarantees that the atom l is a 
                       % descending atom

    iter = 0;

    while maxprod>0
    
        %------------------------------------------------------------------
        % 1. ULS update : skipped in the first itertion if z_uls is
        %    provided as input
        %------------------------------------------------------------------

        if (iter ~=0 | nargin == 6),
            
            % ULS update:
            % Compute the ULS solution corresponding to V U l
            % from the ULS solution zV corresponding to V
        
            if isempty(V),
                z_uls = Hy(l);
                theta = 1;
                err2 = err2 - z_uls*z_uls;
            else,
                phi = HH(V,l);
                thetaphi = theta*phi;
                delta = 1/(1 - phi'*thetaphi);
                nu = phi'*zV-Hy(l);
                %
                err2 = err2 - delta*nu*nu;
                z_uls = [zV;0] + delta*nu*[thetaphi;-1];
                theta = [theta, zeros(k,1); zeros(1,k+1)] + ...
                        delta*[thetaphi;-1]*[thetaphi',-1];
            end
        
            if (iter>=1 && VERB),
                lst_add_rm = [lst_add_rm, l];   
            end
        end
            
        %------------------------------------------------------------------
        % 2. NNLS computation from the ULS solution
        %------------------------------------------------------------------

        % z_uls is not necessarily >= 0, search for a nonnegative solution z
        V = [V, l];      % same size as z_uls
        k = k+1;
        zV = [zV; 0];    % current NNLS solution for S
        
        while min(z_uls)<0
            
            neg = find(z_uls<0);

            [alpha,j] = min(zV(neg)./(zV(neg)-z_uls(neg)));
            zV = zV + alpha*(z_uls-zV);
            idx_desel = neg(j);    % index in V of the atom to leave the support
            
            % Deselection of atom indexed by l
            delta = theta(idx_desel,idx_desel);
            z_l = z_uls(idx_desel); 
            err2 = err2 + z_l*z_l/delta;
            % Compute intermediate vector thetal = theta(:-j,j):
            thetal = theta(:,idx_desel); 
            thetal(idx_desel) = [];
            %
            z_uls(idx_desel) = [];
            z_uls = z_uls - (z_l/delta)*thetal;
            %
            theta(idx_desel,:) = []; 
            theta(:,idx_desel) = [];
            theta = theta - thetal*thetal'/delta;
            
            if (VERB),
                lst_add_rm = [lst_add_rm, -V(idx_desel)];    
            end;
            
            % Atom removal
            V(idx_desel) = []; 
            zV(idx_desel) = []; 
            k = k-1;
        end
    
        zV = z_uls;
        
        Vbar = setdiff(Vall,V);

        % OMP-like selection of atoms: compute inner products 
        % H(:,Vbar)'r = H(:,Vbar)'y-H(:,Vbar)'*H(:,V)*zV
        if ~isempty(Vbar),
            prod = Hy(Vbar)-HH(Vbar,V)*zV;
            [maxprod,idx] = max(prod);
            l = Vbar(idx);
        else,
            maxprod = 0;
        end;
        
        iter = iter+1;
    end

