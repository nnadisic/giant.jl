% realH = full(sprand(4,8,0.5));
% W = rand(6,4);
% M = W * realH;
%
% [myH,t,e] = matrixnnols(M,W,2);
%
% display(e)

clear all;

colsparsity = @(m)sum(sum([m > 10e-3]))/size(m,2);

nbiter=10;

% CBCL
load('cbclim.mat');
load('cbclim_H.mat');
k = 3;
times = zeros(nbiter,1);
for run=1:nbiter
    [W,t,e] = matrixnnols(M',H', k);
    times(run) = t;
end
affichage(W',7,19,19,1);
disp("CBCL")
display([median(t) 100*e colsparsity(W)])
export_fig out-cbcl-nnols.png;
close;


% Frey
load('frey.mat');
load('frey_H.mat');
k = 6;
times = zeros(nbiter,1);
for run=1:nbiter
    [W,t,e] = matrixnnols(M',H', k);
    times(run) = t;
end
affichage(W',6,20,28,1);
disp("Frey")
display([median(t) 100*e colsparsity(W)])
export_fig out-frey-nnols.png;
close;


% KULS
load('kuls.mat');
load('kuls_H.mat');
k = 3;
times = zeros(nbiter,1);
for run=1:nbiter
    [W,t,e] = matrixnnols(M',H', k);
    times(run) = t;
end
affichage(W',5,64,64,1);
disp("KULS")
display([median(t) 100*e colsparsity(W)])
export_fig out-kuls-nnols.png;
close;


% Jasper
load('jasper.mat');
load('jasper-gt-r4.mat');
k = 2;
times = zeros(nbiter,1);
for run=1:nbiter
    [H,t,e] = matrixnnols(Y,M, k);
    times(run) = t;
end
affichage(H',4,100,100,1);
disp("Jasper")
display([median(t) 100*e colsparsity(H)])
export_fig out-jasper-nnols.png;
close;


% Samson
load('samson.mat');
load('samson-gt-r3.mat');
k = 2;
times = zeros(nbiter,1);
for run=1:nbiter
    [H,t,e] = matrixnnols(V,M, k);
    times(run) = t;
end
affichage(H',3,95,95,1);
disp("Samson")
display([median(t) 100*e colsparsity(H)])
export_fig out-samson-nnols.png;
close;


% Urban
load('Urban.mat');
load('Urban_Ref.mat');
k = 2;
times = zeros(nbiter,1);
for run=1:nbiter
    [H,t,e] = matrixnnols(A',References', k);
    times(run) = t;
end
affichage(H',6,307,307,1);
disp("Urban")
display([median(t) 100*e colsparsity(H)])
export_fig out-urban-nnols.png;
close;


% Cuprite
load('Cuprite.mat');
k = 4;
times = zeros(nbiter,1);
for run=1:nbiter
    [H,t,e] = matrixnnols(x,U, k);
    times(run) = t;
end
affichage(H',4,250,191,1);
disp("Cuprite")
display([median(t) 100*e colsparsity(H)])
export_fig out-cuprite-nnols-k4.png;
close;


% San Diego
% Maybe no?
