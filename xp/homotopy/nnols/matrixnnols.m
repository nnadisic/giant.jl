function [H,time,relerror] = matrixnnols(M,W,k)
%MATRIXNNOLS Wrapper of NNOLS, to use for multiple right-hand sides NNLS
% Normalize columns of W
normc = @(m)m./sqrt(sum(m.^2));
W = normc(W);
% Init H
n = size(M,2);
r = size(W,2);
H = zeros(r,n);
tic;
% Loop on columns
for j=1:n
    [hj,~,~] = nnols(M(:,j), W, k);
    H(:,j) = full(hj);
end
time = toc;
relerror = norm(M-W*H,'fro')/norm(M,'fro');
end
