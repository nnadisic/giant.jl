% function [x,S,err2] = nnols(y,H,K,varargin)
%
% This program is a supplementary material to the paper:
%
% Non-Negative Orthogonal Greedy algorithms, by T.T. Nguyen,
% J. Idier, C. Soussen, and E.-H. Djermoune
%
% https://hal.archives-ouvertes.fr/hal-02049424
%
% Minimize || y-Hx ||_2^2 s.t. ||x||_0 <= K, x>=0
%
% using Non-negative Orthogonal Least Squares 
% [Yaghoobi et al, 2015] integrating Support Shrinkage.
%
% Fast implementation using Active set-NNLS [Lawson et al, 1974] with warm start
%
% INPUTS
%
% y: data signal, vector of size mx1
% H: normalized dictionary, matrix of size mxn
% K: desired sparsity level
% 
% OPTIONAL INPUTS
%
% epsilon : additional stopping condition || y-Hx||_2 < epsilon. 
%           epsilon = 0 by default
% VERB:  =1 for verbose mode (many prints on screen), = 0 (no print) by default
%
% OUTPUTS:
%
% x: solution, vector of size nx1
% S: support of solution, vector of size 1 x k with k <= K
% err2: related squared error || y - Hx ||_2^2 (scalar)
%
% Date: Feb. 5, 2019
% Last modified: Feb. 5, 2019
% Authors: T.T. Nguyen, J. Idier, C. Soussen, and E.-H. Djermoune
%----------------------------------------------------------------------
% ******  Copyright (c) 2019   *******
% ******  License CeCILL       *******
%----------------------------------------------------------------------

function [x,S,err2] = nnols(y,H,K,varargin)

if nargin==3,
    epsilon = 0;
    VERB = 0;
elseif nargin==4,
    epsilon = varargin{1};
    VERB = 0;
elseif nargin==5,
    epsilon = varargin{1};
    VERB = varargin{2};
end

roundoff = eps;    % for comparisons with 0

Hy = H'*y;        
y2 = y'*y;        

% current support 
S = []; 
k = 0;    % length(S)

HsH = [];        % matrix H(:,S)'*H 

Sall = 1:size(H,2);   % all atoms

xs = [];         % coefficients of selected atoms
theta = [];      % inverse of the Gram matrix H(:,S)'*H(:,S)
err2 = y2;       % squared error

iter = 0;

if VERB==1
    fprintf('\n\nIter.\t MOVE \t MSE\n');
    fprintf('-------------------------------\n');
    fprintf('%d:\t\t%1.5e\n',iter,err2);
end


while length(S)< K && (err2-epsilon^2)>roundoff
    
    iter = iter+1;
    
    if (k==0), % first iteration
        prod = Hy;
        descend = prod>0;     % 1 for descending atoms 
        prod = prod(descend); % keep only the descending atoms
        
        if isempty(prod),     % no descending atom
            break;
        end;

        % Type-I pruning: remove non-descending candidate atoms
        Sbar = Sall(descend);

        theta = 1;
        % Compute maximum decrease of squared error for all descending atoms:
        [dE,idx] = max(prod);
        err2 = err2-dE*dE;      % Update of squared error
        l = Sbar(idx);          % selected atom
        xs = prod(idx);         % coefficient
        
        % Update S, HsH
        S = l;
        k = 1;
        HsH = H(:,l)'*H;
        lst_add_rm = [];

    else % from second iteration
         
        %------------------------------------------------------------------
        % 1. Check descending atoms
        %------------------------------------------------------------------
        Sbar = setdiff(Sall,S);
        
        % Check descending atoms by computing the inner products with residual
        prod = Hy(Sbar)-HsH(:,Sbar)'*xs;
        
        descend = prod>0;     % 1 for descending atoms 
        prod = prod(descend); % keep only the descending atoms
        
        if isempty(prod),  % no descending atom
            break;
        end;
        
        % Type-I pruning: remove non-descending candidate atoms
        Sbar = Sbar(descend);
        
        %------------------------------------------------------------------
        % 2. Compute in parallel the ULS solutions and relative squared 
        % residual errors related to the insertion of atoms for all 
        % candidate atoms
        %------------------------------------------------------------------
        phi_vect = HsH(:,Sbar);         % matrix  k x (n-k)
        thetaphi_vect = theta*phi_vect; % matrix k x (n-k)

        % vectors of size (n-k):
        delta_vect = 1./(1 - sum(phi_vect.*thetaphi_vect,1)'); 
        nu_vect = phi_vect'*xs-Hy(Sbar);  
        err2_vect = err2 - delta_vect.*nu_vect.*nu_vect;  

        % matrix of ULS solutions (k x (n-k))         
        xs_vect = repmat([xs;0],1,length(Sbar))+...
            repmat((delta_vect.*nu_vect)',length(xs)+1,1).*...
            [thetaphi_vect;-ones(1,length(Sbar))];
        
        
        % Check the positive / non-positive status of supports S U {i}
        % - Indices of non-positive supports in Sbar:
        non_pos_in_Sbar = find(min(xs_vect) < 0);     
        % - Indices of positive supports in Sbar:
        pos_in_Sbar = setdiff(1:length(Sbar),non_pos_in_Sbar); 
        
        lst_add_rm = [];
        is_pos = 0;
        
        %------------------------------------------------------------------
        % 3. Find best positive support S U {i}
        %------------------------------------------------------------------
        %--- COMPUTE THE ULS SOLUTION FOR THE BEST POSITIVE SUPPORT IN --
        %--- TERMS OF SQUARED RESIDUAL ERRORS                          --
        if ~isempty(pos_in_Sbar)      
            is_pos = 1;   % There exist positive supports
            
            % Update err2 by computiong best error for positive supports
            % S U {i}
            [err2, idx_pos] = min(err2_vect(pos_in_Sbar));
            l = Sbar(pos_in_Sbar(idx_pos)); % best support among positive supports
        end;
            
        %------------------------------------------------------------------
        % 4. Find best non-positive support S U {i}
        %------------------------------------------------------------------
        %--- COMPUTE THE NNLS SOLUTION FOR NON-POSITIVE SUPPORTS --
        %--- AND SAVE THE BEST ONE                               --
        if ~isempty(non_pos_in_Sbar),                 
            % We will first explore the supports having the unconstrained 
            % least squared error to improve the effectiveness of on-the-fly 
            % type-II pruning:
            [~, ind_sort] = sort(err2_vect(non_pos_in_Sbar));

            %------------------------------------------------------------------
            % 4.2 call NNLS for remaining non-positive supports
            %------------------------------------------------------------------
            for ii =1:length(non_pos_in_Sbar),
                % first explore the supports having the least
                % unconstrained squared error to improve the
                % effectiveness of on-the-fly type-II pruning:
                i = ind_sort(ii);          
                i_Sbar = non_pos_in_Sbar(i);

                l_temp = Sbar(i_Sbar);
                S_augm = [S, l_temp];      % augmented subset
                err2_uls = err2_vect(i_Sbar);
                
                %---------------------------------------------------------------
                % 4.1 Type-II pruning test: keep only atoms resulting error 
                % smaller than current best error for positive supports
                %--------------------------------------------------------------
                if (err2_uls > err2),
                    % Such candidate atoms cannot improve err2_best
                    continue;
                end
                
                x_uls = xs_vect(:,i_Sbar);
                
                thetaphi_temp = thetaphi_vect(:,i_Sbar);

                theta_temp = [theta,zeros(k,1); zeros(1,k+1)] +...
                    delta_vect(i_Sbar)*[thetaphi_temp;-1]*[thetaphi_temp',-1];
                
                % Modification of new row of HsH related to candidate atom
                HsH(k+1,:) = H(:,l_temp)'*H;
                
                [xs_temp,V_temp,theta_temp,err2_nnls,lst_add_rm_temp]...
                    = AS_NNLS(Hy(S_augm),HsH(:,S_augm),xs,theta_temp,err2_uls,VERB,x_uls);
                
                % Save the best candidate atom tested so far among
                % positive and non-positive supports all-together
                if (err2_nnls < err2)
                    is_pos = 0;
                    l = l_temp;        % best candidate atom so far
  
                    S_save =  S_augm;  % uncompressed, save for print if VERB=1
                    err2 = err2_nnls;  % best squared error so far
                    xs_best = xs_temp;
                    V_best = V_temp;
                    HsH_best = HsH(k+1,:); 
                    theta_best = theta_temp;
                    lst_add_rm = lst_add_rm_temp;
                end
            end
        end

        %------------------------------------------------------------------
        % 5. Atom selection + compression
        %------------------------------------------------------------------
        if (is_pos),  % the selected atom corresponds to a positive support
            S = [S, l];
            xs = xs_vect(:,pos_in_Sbar(idx_pos));
            
            thetaphi_pos = thetaphi_vect(:,pos_in_Sbar(idx_pos));
            delta_pos = delta_vect(pos_in_Sbar(idx_pos));
            theta = [theta,zeros(k,1); zeros(1,k+1)] +...
                    delta_pos*[thetaphi_pos;-1]*[thetaphi_pos',-1];
            HsH(k+1,:) = H(:,l)'*H;   % add new row to old HsH 
            k = k+1;
        else,
            S = [S, l];
            S = S(V_best);            % support compression
            xs = xs_best;
            theta = theta_best;
            HsH(k+1,:) = HsH_best;   % add new row to old HsH 
            k = length(S);
            HsH = HsH(V_best,:);      % support compression, HsH has now k rows
        end;
    end;

    if (VERB==1)
        fprintf('-------------------------------\n');
        if (isempty(lst_add_rm)),
            fprintf('%d:\t+%d\t%1.5e\n',iter,l,err2);
        else,
            fprintf('%d:\t+%d\t\n',iter,l);
            for i=1:(length(lst_add_rm)-1)
                if (lst_add_rm(i)>0)
                    fprintf('\t+%d\t\n',S_save(lst_add_rm(i)));
                else % lst_add_rm(i) < 0
                    fprintf('\t-%d\t\n',S_save(-lst_add_rm(i)));
                end
            end
            i=length(lst_add_rm);
            if (lst_add_rm(i)>0)
                fprintf('\t+%d\t%1.5e\n',S_save(lst_add_rm(i)),err2);
            else, 
                % Case lst_add_rm(i) < 0:
                fprintf('\t-%d\t%1.5e\n',S_save(-lst_add_rm(i)),err2);
            end
        end
    end
end

x =spalloc(size(H,2),1,nnz(xs)); x(S) = xs;

if VERB==1
    fprintf('-------------------------------\n\n');
end

