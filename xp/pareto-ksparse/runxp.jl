# Testing pareto-arborescent on facial and hyperspectral images

using GIANT

using LinearAlgebra
using MAT
using Statistics

# Load data metadata and parameters
include("../xpmetadata.jl")


function runxp(p::XpParams; kmin::Integer=1, nbruns::Integer=10)
    # Announce
    println("\nProcessing $(uppercase(p.name))")

    # Read data and dictionnary
    data = matopen(p.datafile)
    M = Array(read(data, p.datavarname))
    dict = matopen(p.dictfile)
    W = Array(read(dict, p.dictvarname))

    # Transpose data if necessary
    if p.transposedata
        M = M'
    end
    if p.transposedict
        W = W'
    end

    # Get rank and sparsity target
    r = size(W,2)
    colk = p.k
    matk = p.matk

    # One run to compile all functions, get errors, sparsity and images
    # Run active set (no sparsity constraint)
    asH, _ = ksparse_mnnls(M, W, r)
    aserr = norm(M - W * asH) / norm(M)
    asspar = colsparsity(asH)

    # Run arborescent k-sparse
    aksH, _ = ksparse_mnnls(M, W, colk)
    akserr = norm(M - W * aksH) / norm(M)
    aksspar = colsparsity(aksH)

    # Run Homotopy budget
    hbH = homotopymatrix(M, W, matk, assignglobal)
    hberr = norm(M - W * hbH) / norm(M)
    hbspar = colsparsity(hbH)

    # Run arborescent + matrix-wise selection
    amwsH = ksparsematrix(M, W, matk)
    amwserr = norm(M - W * amwsH) / norm(M)
    amwsspar = colsparsity(amwsH)

    # Save abundance maps
    npr = p.nbpixelsrow
    npc = p.nbpixelscol
    nir = p.nbimgperrow
    bw = p.bw
    displayabundancemap(asH, npr, npc, "out-$(p.name)-as.png", bw=bw, nbimgperrow=nir)
    displayabundancemap(aksH, npr, npc, "out-$(p.name)-arbocolk.png", bw=bw, nbimgperrow=nir)
    displayabundancemap(hbH, npr, npc, "out-$(p.name)-hb.png", bw=bw, nbimgperrow=nir)
    displayabundancemap(amwsH, npr, npc, "out-$(p.name)-arbomat.png", bw=bw, nbimgperrow=nir)


    # Various runs to get better runtime estimation
    astime, akstime, hbtime, amwstime = [zeros(nbruns) for _ in 1:4]
    for run in 1:nbruns
        print("$(run) ")

        # Run active set (no sparsity constraint)
        astime[run] = @elapsed asH, _ = ksparse_mnnls(M, W, r)
        # Run arborescent k-sparse
        akstime[run] = @elapsed aksH, _ = ksparse_mnnls(M, W, colk)
        # Run Homotopy budget
        hbtime[run] = @elapsed hbH = homotopymatrix(M, W, matk, assignglobal)
        # Run arborescent + matrix-wise selection
        amwstime[run] = @elapsed amwsH = ksparsematrix(M, W, matk)
    end

    println("")
    println("Algo\tAS\tAks\tHb\tAmws")
    println("Time\t$(round(median(astime),digits=2))\t$(round(median(akstime),digits=2))\t$(round(median(hbtime),digits=2))\t$(round(median(amwstime),digits=2))")
    println("Error\t$(round(aserr*100,digits=3))\t$(round(akserr*100,digits=3))\t$(round(hberr*100,digits=3))\t$(round(amwserr*100,digits=3))")
    println("Spar\t$(round(asspar,digits=2))\t$(round(aksspar,digits=2))\t$(round(hbspar,digits=2))\t$(round(amwsspar,digits=2))")
end



runxp(paramsamson)
runxp(paramsamson_b)
runxp(paramjasper)
runxp(paramjasper_b)
runxp(paramurban)
runxp(paramurban_b)
runxp(paramcuprite, nbruns=1)
