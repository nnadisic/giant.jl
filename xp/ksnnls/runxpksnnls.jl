# Experiments to compare arbo with MIP solver in k-sparse NNLS problems

using GIANT
using LinearAlgebra
using Statistics
using Plots


function generatedata(m, r, k, addnoise=true; percentnoise=0.01)
    # Generate data
    A = rand(m, r)
    realx = rand_ksparse_vect(r, k)
    b = A*realx
    # Add noise if needed
    if addnoise==true
        noise = randn(m)
        noise ./= norm(noise)
        noise .*= norm(b) * percentnoise
        b .+= noise
        b[b .< 0] .= 0 # ensure b is nonnegative
    end
    return A, b
end


function runallalgos(A, b, k, bigm)
    # Brute force
    print(" bf...")
    bftime = @elapsed bfx, _ = bruteforce(A'*A, A'*b, k)
    # Arbo
    print(" arbo...")
    arbotime = @elapsed arbox, _ = arborescent(A'*A, A'*b, k)
    # MIP
    println(" mip...")
    miptime = @elapsed mipx = mipksnnls(A, b, k, bigm)
    # Return times
    return bftime, arbotime, miptime
end


function xp_var_k(nbmc, m, r, kmin, kmax; bigm=2)
    println()
    results = zeros(kmax-kmin+1, 3, nbmc)
    for (i, k) in enumerate(kmin:kmax)
        for mc in 1:nbmc
            print("m=$(m) r=$(r) k=$(k) - mc $(mc)/$(nbmc)")
            A, b = generatedata(m, r, k)
            results[i, 1:3, mc] .= runallalgos(A, b, k, bigm)
        end
    end
    medianresults = median(results, dims=3)[:,:,1]
    plot(kmin:kmax, medianresults,
         title="Running time in seconds m=$(m) r=$(r)",
         xlabel="k",
         label=["Bruteforce" "Arbo" "CPLEX"])
    gui()
    return nothing
end


function xp_var_m(nbmc, r, k, mmin, mmax, mstep; bigm=2)
    println()
    mvalues = collect(mmin:mstep:mmax)
    results = zeros(length(mvalues), 3, nbmc)
    for (i, m) in enumerate(mvalues)
        for mc in 1:nbmc
            print("m=$(m) r=$(r) k=$(k) - mc $(mc)/$(nbmc)")
            A, b = generatedata(m, r, k)
            results[i, 1:3, mc] .= runallalgos(A, b, k, bigm)
        end
    end
    medianresults = median(results, dims=3)[:,:,1]
    plot(mvalues, medianresults,
         title="r=$(r) k=$(k)",
         xlabel="m",
         ylabel="Running time in seconds",
         label=["Bruteforce" "Arbo" "CPLEX"])
    gui()
    return nothing
end



function xp_var_r(nbmc, m, rmin, rmax, rstep; bigm=2)
    println()
    rvalues = collect(rmin:rstep:rmax)
    results = zeros(length(rvalues), 3, nbmc)
    for (i, r) in enumerate(rvalues)
        k = Int(r/2)
        for mc in 1:nbmc
            print("m=$(m) r=$(r) k=$(k) - mc $(mc)/$(nbmc)")
            A, b = generatedata(m, r, k)
            results[i, 1:3, mc] .= runallalgos(A, b, k, bigm)
        end
    end
    medianresults = median(results, dims=3)[:,:,1]
    plot(rvalues, medianresults,
         title="m=$(m) k=r/2",
         xlabel="r",
         ylabel="Running time in seconds",
         label=["Bruteforce" "Arbo" "CPLEX"])
    gui()
    return nothing
end



function runmips(A, b, k, bigm)
    # Cplex
    print(" cplex...")
    cplextime = @elapsed cplexx = mipksnnls(A, b, k, bigm)
    # Gurobi
    println(" gurobi...")
    grbtime = @elapsed grbx = mipksnnls(A, b, k, bigm, "gurobi")
    # Return times
    return cplextime, grbtime
end


function comparemipsolvers(nbmc, m, rmin, rmax, rstep; bigm=2)
    println()
    rvalues = collect(rmin:rstep:rmax)
    results = zeros(length(rvalues), 2, nbmc)
    for (i, r) in enumerate(rvalues)
        k = Int(r/2)
        for mc in 1:nbmc
            print("m=$(m) r=$(r) k=$(k) - mc $(mc)/$(nbmc)")
            A, b = generatedata(m, r, k)
            results[i, 1:2, mc] .= runmips(A, b, k, bigm)
        end
    end
    medianresults = median(results, dims=3)[:,:,1]
    plot(rvalues, medianresults,
         title="m=$(m) k=r/2",
         xlabel="r",
         ylabel="Running time in seconds",
         label=["CPLEX" "Gurobi"])
    gui()
    return nothing
end


# Run experiments
# xp_var_k(100, 200, 20, 1, 19)
# xp_var_m(100, 12, 6, 12, 192, 12)
# xp_var_r(100, 100, 10, 20, 2)
comparemipsolvers(100, 100, 10, 50, 2)
