# Experiments to compare arbo with MIP solver in k-sparse NNLS problems in hyperspectral unmixing

using GIANT
using MAT
using LinearAlgebra
using Statistics


# Load data metadata and parameters
include("../xpmetadata.jl")


function runxpksnnls_hsu(p::XpParams, nbruns::Integer=10; bigm::Integer=2)
    # Announce
    println("\nProcessing $(uppercase(p.name))")

    # Read data and dictionnary
    data = matopen(p.datafile)
    M = Array(read(data, p.datavarname))
    dict = matopen(p.dictfile)
    W = Array(read(dict, p.dictvarname))

    # Transpose data if necessary
    if p.transposedata
        M = M'
    end
    if p.transposedict
        W = W'
    end

    # Get rank and sparsity target
    r = size(W,2)
    n = size(M,2)
    colk = p.k
    matk = p.matk

    bfH, arboH, mipH = [zeros(r, n) for _ in 1:3]

    # Various runs to get better runtime estimation
    bftime, arbotime, miptime = [zeros(nbruns) for _ in 1:3]
    for run in 1:nbruns
        print("$(run) ")
        # bftime[run] = @elapsed bfH, _ = ksparse_mnnls(M, W, colk, solver=bruteforce)
        arbotime[run] = @elapsed arboH, _ = ksparse_mnnls(M, W, colk, solver=arborescent)
        miptime[run] = @elapsed mipH = mipksnnlsmatrix(M, W, colk, bigm)
    end

    bferr, arboerr, miperr = [norm(M - W * H)/norm(M) for H in [bfH, arboH, mipH]]
    bfspar, arbospar, mipspar = [colsparsity(H) for H in [bfH, arboH, mipH]]

    println("")
    println("Algo\tBf\tArbo\tCplex")
    println("Time\t$(round(median(bftime),digits=2))\t$(round(median(arbotime),digits=2))\t$(round(median(miptime),digits=2))")
    println("Error\t$(round(bferr*100,digits=3))\t$(round(arboerr*100,digits=3))\t$(round(miperr*100,digits=3))")
    println("Spar\t$(round(bfspar,digits=2))\t$(round(arbospar,digits=2))\t$(round(mipspar,digits=2))")
end


runxpksnnls_hsu(paramsamson)
runxpksnnls_hsu(paramjasper, bigm=8000)
runxpksnnls_hsu(paramurban)
runxpksnnls_hsu(paramcuprite)
